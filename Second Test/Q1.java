import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array");
		int n = sc.nextInt();
		System.out.println("Enter a sorted array of size "+n);
		int[] arr = new int[5];
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Array in wave-like form: ");
		for(int i=0; i<n-1; i++) {
			if(i%2==0) {
				int temp = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp;
			}
		}
		for(int i=0; i<n; i++) {
			System.out.print(arr[i]+" ");
		}

	}

}

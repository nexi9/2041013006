import java.util.Scanner;

public class Q2 {
	
	static int maxElement(int[] x, int n) {
		int max=x[0];
		for(int i=0; i<n; i++) {
			if(x[i]>max) {
				max = x[i];
			}
		}
		return max;
	}
	
	static int minElement(int[] x, int n) {
		int min=x[0];
		for(int i=0; i<n; i++) {
			if(x[i]<min) {
				min = x[i];
			}
		}
		return min;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of 1st array");
		int m = sc.nextInt();
		System.out.println("Enter elements of 1st array");
		int[] arr = new int[m];
		for(int i=0; i<m; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter size of 2nd array");
		int n = sc.nextInt();
		System.out.println("Enter elements of 2nd array");
		int[] brr = new int[n];
		for(int i=0; i<n; i++) {
			brr[i] = sc.nextInt();
		}
		int min = minElement(arr,m);
		System.out.println("Minimum element of 1st array: "+min);
		int max = maxElement(brr,n);
		System.out.println("Maximum element of 2nd array: "+max);
		
		int pro = max*min;
		System.out.println("Their product = "+pro);
		
		

	}

}

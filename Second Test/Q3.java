import java.util.Scanner;

public class Q3 {

	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter the number of rows ");
		 int row = sc.nextInt();
		 System.out.println("Enter number of symbols");
		 int sym = sc.nextInt();
		 for(int i=0; i<row; i++) {
			 for(int j=0; j<sym; j++) {
				 System.out.print("/"+"\\"+"");
				
			 }
			 System.out.println();
			 System.out.print("|");
			 for(int j=0; j<sym+1; j++) {
				 System.out.print(" ");
			 }
			 System.out.print("|");
			 System.out.println();
		 }

	}

}


public class Q86 {

	public static void main(String[] args) {
		for(int i=1;i<1000;i++)
		{
			int square=i*i;
			int count=0;
			int temp=i;
			while(temp!=0)
			{
				count++;
				temp/=10;
			}
			// check the divide part
			int divide_part=(int)Math.pow(10, count);
			int first_part=square/divide_part;
			int second_part=square%divide_part;
			int sum=square/divide_part+square%divide_part;
			if(sum==i)
			{
				System.out.println(i+"\t"+square+"\t"+first_part+"+"+second_part);
			}
		}

	}

}

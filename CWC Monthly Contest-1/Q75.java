import java.util.Scanner;

public class Q75 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int val = 1;
		for(int i=2; i<=n; i=i+2) {
			val *= i;
		}
		System.out.println("Double factorial = "+val);

	}

}

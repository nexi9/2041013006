import java.util.Scanner;

public class Q35 {
	
	static int fibo(int n) {
		double cons = (1 + Math.sqrt(5))/2;
		return (int)(Math.pow(cons, n)/Math.sqrt(5));
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the term you want to find");
		int n = sc.nextInt();
		int res = fibo(n);
		System.out.println("The "+n+"th term is "+res);

	}

}

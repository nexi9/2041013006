
public class Q100 {
	
    static int toBinary(int n){        
	     int binary = 0;   
	     while(n > 0){    
	          int rem = n%2;
	          binary = binary*10 + rem;
	          n = n/2;
	     } 
	     return binary;
	}
    
    static boolean isPalindrome(int n) {
    	int temp=n;
    	int num=0;
    	while(n>0) {
    		int rem = n%10;
    		num = num*10 + rem;
    		n=n/10;
    	}
    	if(temp==num) {
    		return true;
    	}
    	return false;
    }
	
	

	public static void main(String[] args) {
		long sum = 0;
		for(int i=0; i<1000000; i++) {
			int bin = toBinary(i);
			if(isPalindrome(i)==true && isPalindrome(bin)==true) {
				sum+=i;
			}
		}
		System.out.println("Sum = "+sum);

	}

}

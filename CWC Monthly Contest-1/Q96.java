
public class Q96 {

	public static void main(String[] args) {
		long sum = 0;
		for(int i=1; i<=1000; i++) {
			int num = (int) Math.pow(i, i);
			sum += num;
		}
		int count=0;
		while(sum>0) {
			if(count==10) {
				break;
			}
			long rem = sum%10;
			count++;
			System.out.print(rem+",");
			sum = sum/10;
		}

	}

}

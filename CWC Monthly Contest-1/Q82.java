import java.util.Scanner;

public class Q82 {
	
	static int transPoint(int[] n) {
		int val = 1;
		for(int i=0; i<n.length-1; i++) {
			if(n[i]!=n[val]) {
				return val;
			}	
			val++;
		}
		return -1;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array");
		int n = sc.nextInt();
		int[] arr = new int[n];
		System.out.println("Enter sorted array of 0s and 1s");
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		int p = transPoint(arr);
		if(p>=0) {
			System.out.println("Transition point = "+p);
		} else {
			System.out.println("-1");
		}
		

	}

}

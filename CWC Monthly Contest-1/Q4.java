
class Point {
	 int x;
	 int y;

	Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	static int distance(Point p1, Point p2) {
		return (p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y);
	}
	
	static boolean isRectangle(Point p1, Point p2, Point p3, Point p4) {
		int d1 = distance(p1, p2);
		int d2 = distance(p2, p3);
		int d3 = distance(p3, p4);
		int d4 = distance(p1, p4);
		
		if(d1 == d3 && d2 == d4 ) {
			return true;
		} else {
		    return false;
		}
	}
}

public class Q4 {

	public static void main(String[] args) {
		Point p1 = new Point(0,10);
		Point p2 = new Point(0,0);
		Point p3 = new Point(10,0);
		Point p4 = new Point(10,10);
		
		boolean ans = Point.isRectangle(p1,p2,p3,p4);
		
		if(ans==true) {
			System.out.println("It forms a rectangle");
		} else {
			System.out.println("it doesn't form a rectangle");
		}
		

	}

	

}

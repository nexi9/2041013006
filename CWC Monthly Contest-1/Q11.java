
import java.util.Scanner;

public class Q11 {
	static int xi;
	static int xf;
	static int yi;
	static int yf;
	
	double distance(int xi, int xf , int yi, int yf) {
		this.xi = xi;
		this.xf = xf;
		this.yi = yi;
		this.yf = yf;
		double d = Math.sqrt(Math.pow(xf-xi, 2)+Math.pow(yf-yi, 2));
		return d;
	}
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Q11 ob = new Q11();
		
		System.out.println("Enter the total no. of paths that you want to compare for:");
		int n = sc.nextInt();
		
		for(int i=0; i<n; i++) {
		if(n==1||n%10==1) {
			System.out.println("Enter the constraints for the "+n+"st path:");
		} else if(n==2||n%10==2){
			System.out.println("Enter the costraints for the "+n+"nd path:");
		} else if(n==3||n%10==3) {
			System.out.println("Enter the constraints for the "+n+"rd path:");
		} else {
			System.out.println("Enter the constraints for the "+n+"th path:");
		}
		System.out.println("Enter x and y coordinates of initial position:");
		  xi = sc.nextInt();
		  yi = sc.nextInt();
		System.out.println("Enter x and y coordinates of final position");
		  xf = sc.nextInt();
		  yf = sc.nextInt();
		  System.out.println(ob.distance(xi,xf,yi,yf)+" units will be covered in path "+(i+1));
		  
		}
		
		
		
		
		

	}

}


import java.util.Scanner;

public class Q44 {
	
	static boolean areEqual(int[] x, int[] y) {
		for(int i=0; i<x.length; i++) {
			if(x[i]!=y[i]) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int n = sc.nextInt();
		System.out.println("Its binary form is");
		int[] binary = new int[100];
		int[] binaryRev = new int[100];
		int i = 0;
		while(n>0) {
			binary[i] = n%2;
			n = n/2;
			i++;
		}
		int k = 0;
		for(int j=i-1; j>=0; j--) {
			System.out.print(binary[j]);
			binaryRev[k] = binary[j];
			k++;
		}
		System.out.println();
		
		
		if(areEqual(binary,binaryRev)==true) {
			System.out.println("Binary form is palindrome");
		} else {
			System.out.println("Binary form is not palindrome");
		}
		
		

	}

}

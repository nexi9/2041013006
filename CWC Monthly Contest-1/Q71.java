import java.util.Scanner;

public class Q71 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("N = ");
		int n = sc.nextInt();
		int arr[][]=new int[n][n];
		for(int i=0;i<n;i++)
		{
			for(int j=1;j<=n;j++)
			{
				if((i+j)%n==0)
				{
					System.out.print(n+" ");
				}
				else
				{
					System.out.print((i+j)%n+" ");
				}
			}
			System.out.println();
		}

	}

}

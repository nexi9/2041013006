import java.util.Scanner;

public class Q65 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number you want to check");
		int n = sc.nextInt();
		boolean[] num = new boolean[10];
		int count = 0;
		while(n>0) {
			
			if(num[n%10]) {
				System.out.println("Not a unique number");
			    break;
			}
			num[n%10] = true;
			n = n/10;
			
		}
		if(n==0) {
		System.out.println("Unique number");
		}
	}

}

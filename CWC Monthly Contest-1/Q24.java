import java.util.Scanner;

public class Q24 {
	
	static void evenOdd(int[] x)
    {
        
        int left = 0;
        int right = x.length - 1;
        while (left < right)
        {
            if (x[left]%2 == 0 && left < right)
                left++;
            
            while (x[right]%2 == 1 && left < right)
                right--;
 
            if (left < right)
            {
                int temp = x[left];
                x[left] = x[right];
                x[right] = temp;
                left++;
                right--;
            }
        }
    }   
 

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array");
		int n = sc.nextInt();
		System.out.println("Enter array elements");
		int[] x = new int[n];
		for(int i=0; i<n; i++) {
			x[i] = sc.nextInt();
		}
		System.out.println("After arranging, array is as follows");
		evenOdd(x);
		for(int i=0; i<n; i++) {
			System.out.print(x[i]+" ");
		}

	}

}

import java.util.Scanner;

public class Q77 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of array");
		int n = sc.nextInt();
		int[] arr = new int[n];
		System.out.println("Enter elements of array");
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter difference");
		int num = sc.nextInt();
		int count = 0;
		for(int i=0; i<n-1; i++) {
			for(int j= i+1; j<n; j++) {
				if(Math.abs(arr[i]-arr[j])==num) {
					count++;
				}
			}
		}
		System.out.println("No. of combos present that result in the same difference= "+count);

	}

}

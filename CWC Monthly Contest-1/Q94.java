
public class Q94 {
	
	static long gcd(long x, long y)
	{
	   if(x%y != 0)
	      return gcd(y,x%y);
	   else
	      return y;
	}
	 
	static long lcm(long n)
	{
	    long val = 1;   
	    for (long i = 1; i <= n; i++)
	        val = val*(i/(gcd(val, i)));
	    return val;
	}
	public static void main(String[] args) {
		System.out.println("Smallest positive no. evenly divisible by all nos. from 1 to 20 = "+lcm(20));

	}

}

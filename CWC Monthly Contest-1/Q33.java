import java.util.Arrays;
import java.util.Scanner;

public class Q33 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int N;
		
		for(int i=0; i<T; i++) {
			N=sc.nextInt();
			int[] arr = new int[N];
			for(int j=0; j<N; j++) {
				arr[j] = sc.nextInt();
			}
			Arrays.sort(arr);
			for(int j=0; j<N; j++)
			{
				System.out.print(arr[j]+" ");
			}
			System.out.println();
		}
		
		
			
	}

}



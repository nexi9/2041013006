import java.util.Scanner;

public class Q72 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length");
		int n = sc.nextInt();
		int[] arr = new int[n];
		int[] index = new int[n];
		System.out.println("Enter the elements");
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter indices");
		for(int i=0; i<n; i++) {
			index[i] = sc.nextInt();
		}
		int[] temp = new int[n];
		for(int i=0; i<n; i++) {
			temp[index[i]] = arr[i];
		}
		for(int i=0; i<n; i++) {
			arr[i] = temp[i];
			index[i] = i;
		}
		System.out.println("arr[] = ");
		for(int i=0; i<n; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		System.out.println("index[] = ");
		for(int i=0; i<n; i++) {
			System.out.print(index[i]+" ");
		}

	}

}

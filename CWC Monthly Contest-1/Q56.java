import java.util.Scanner;

public class Q56 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int n = sc.nextInt();
		int sumN = 0;
		while(n>0) {
			int rem = n%10;
			sumN = sumN + rem;
			n = n/10;
		}
		int sum = 0;
		while(sumN>0) {
			int rem = sumN%10;
			sum = sum + rem;
			sumN = sumN/10;
		}
		if(sum==1) {
			System.out.println("Magic number");
		} else {
			System.out.println("Not a magic number");
		}

	}

}

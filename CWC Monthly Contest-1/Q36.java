import java.util.Scanner;

public class Q36 {
	
	static int fact(int k) {
		int factorial = 1;
		for(int i=1; i<=k; i++) {
			factorial *= i;
		}
		return factorial;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of n");
		int n = sc.nextInt();
		int[] arr = new int[n];
		System.out.println("Catalan sequence upto n");
		int j=1;
		for(int i=0; i<n ; i++) {
			int A = fact(2*j);//2n!
			int B = fact(j+1);//(n+1)!
			int C = fact(j);//n!
			arr[i] = (A/(B*C));
			j++;
		}
		for(int i=0; i<n; i++) {
			System.out.print(arr[i]+" ");
		}

	}

}

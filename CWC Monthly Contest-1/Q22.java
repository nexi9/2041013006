import java.util.Scanner;

public class Q22 {
	
	static int product(int x, int y) {
		int res = 0;
		while(y != 0) {
			
		
			if((y&1) != 0) {
				res = res+x;
			}
			
			x = x<<1;
			y = y>>1;

		}	
		
		return res;
		
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter two integers");
		int x = sc.nextInt();
		int y = sc.nextInt();
		System.out.println(product(x,y));

	}

}

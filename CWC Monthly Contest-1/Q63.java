import java.util.Scanner;

public class Q63 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int dist = sc.nextInt();
		double pay = 0;
		if(dist>250) {
			int rem = dist - 250;
			pay = rem*0.0125 + 40.00;
		} else {
			pay = dist*0.16;
		}
		System.out.println("P"+pay);

	}

}

import java.util.Scanner;

public class Q7 {
	
	static double PI = 3.14;
   
    static double areaCircle(double a)
    {
        return (PI/4)*a *a;
    }

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter side of square");
        double n = sc.nextDouble();
        System.out.println("Area of circle = "+areaCircle(n));
        
	}

}

import java.util.Scanner;

public class Q81 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter an even size of array");
		int n = sc.nextInt();
		System.out.println("Enter the array elements");
		int[] arr = new int[n];
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		int[] temp1 = new int[n/2];
		int[] temp2 = new int[n/2];
		for(int i=0; i<n/2; i++) {
			temp1[i] = arr[i];
		}
		int val = n/2;
		for(int i=0; i<n/2; i++) {
			temp2[i] = arr[val];
			val++;
		}
		int sum1 = 0, sum2 = 0;
		for(int i=0; i<n/2; i++) {
			sum1 += temp1[i];
			sum2 += temp2[i];
		}
		if(sum1==sum2) {
			System.out.println(0);
		} else {
			System.out.println((int)Math.abs(sum1-sum2));
		}
		

	}

}

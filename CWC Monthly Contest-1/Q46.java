import java.util.Scanner;

public class Q46 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("If the eqn is in the form of ax^2 + bx + c = 0, enter the values of a,b and c");
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();
        double d = Math.sqrt(Math.pow(b, 2)-4*a*c);
        double root1;
        double root2;
        if(d>0) {
        	 root1 = ((-b+d)/(2*a));
        	 root2 = ((-b-d)/(2*a));
        	 System.out.println("root1 = "+root1);
        	 System.out.println("root2 = "+root2);
        } else if(d==0) {
        	root1 = root2 = -b/(2*a);
        	 System.out.println("root1 = root2 = "+root1);
        } else {
        	double real = -b/(2*a);
        	double imag = d/(2*a);
        	 System.out.println("root1 = "+real+"+"+imag+"i");
             System.out.println("root2 = "+real+"-"+imag+"i");
        }
        
        
       
	}

}

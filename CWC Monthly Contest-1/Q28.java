import java.util.Scanner;

public class Q28 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of n");
		int n = sc.nextInt();
		int sum = 0;
		int val = 1;
		for(int i=0; i<n; i++) {
			sum+= Math.pow(val, n-(val-1));
			val++;
		}
		System.out.println("Sum="+sum);

	}

}

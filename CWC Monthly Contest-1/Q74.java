import java.util.Scanner;

public class Q74 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of array");
		int n = sc.nextInt();
		double[] arr = new double[n];
		System.out.println("Enter elements of array");
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextDouble();
		}
		double sum = 0;
		for(int i=0; i<n; i++) {
			sum += 1/arr[i];
		}
		double res = n/sum;
		System.out.println("Harmonic mean = "+res);
		

	}

}

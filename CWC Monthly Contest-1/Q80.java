import java.util.Scanner;

public class Q80 {
	
	static int binary(int n) {
		int bin = 0;
		while(n>0) {
			int rem = n%2;
			bin = bin*10 + rem;
			n = n/2;
		}
		return bin;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter two numbers");
		int m = sc.nextInt();
		int n = sc.nextInt();
		if(binary(m)==binary(n)) {
			System.out.println("True");
		} else {
			System.out.println("False");
		}

	}

}

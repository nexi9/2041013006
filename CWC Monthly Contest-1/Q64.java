import java.util.Scanner;

public class Q64 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the car no:");
		int n = sc.nextInt();
		int temp =n;
		int count = 0;
		while(n>0) {
			int r = n%10;
			count++;
			n = n/10;
		}
		if(count!=4||temp==0||temp<0) {
			System.out.println(temp+" is not a valid car number");
		}
		if(temp>0 && count==4) {
		int sum = 0;
		while(temp>0) {
			int r = temp%10;
			sum = sum+r;
			temp = temp/10;
		}
		if(sum%3==0 || sum%5==0 || sum%7==0) {
			System.out.println("Lucky Number");
		} else {
			System.out.println("Sorry its not my lucky number");
		}
		}
		

	}

}

import java.util.Scanner;

public class Q2 {
	
	static int fibo(int x) {
		if(x==0)
			return 0;
		else if(x==1) 
			return 1;
		else 
			return fibo(x-1)+fibo(x-2);
	}
	
	static int sum(int l, int n) {
		int sum=0;
		for(int i=l+2; i<=n; i++) {
			sum+=fibo(i);
		}
		return sum;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the lower bound");
		int l = sc.nextInt();
		System.out.println("Enter the upper bound");
		int n = sc.nextInt();
		System.out.println("Partial sum=");
		sum(l,n);
		

	}

}

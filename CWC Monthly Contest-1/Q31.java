import java.util.Scanner;

public class Q31 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a 4X4 matrix");
		int[][] arr = new int[4][4];
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				arr[i][j] = sc.nextInt();
			}
		}
		System.out.println("The matrix entered is");
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
		int sum=0;
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				sum += arr[i][j];
				if(i==1&&j==1 || i==1&&j==2
						||i==2&&j==1 || i==2&&j==2) {
					sum -= arr[i][j];
				}
					
			}
		}
		System.out.println("Sum="+sum);

	}

}

import java.util.Scanner;

public class Q57 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter an unsigned integer");
        int n = sc.nextInt();
        int count = 0;
        while(n>0) {
        	int rem = n%10;
        	if(rem==1) {
        		count++;
        	}
        	n = n/10;
        }
        System.out.println("No. of 1s = "+count);
	}

}

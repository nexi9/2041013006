import java.util.Scanner;

public class Q59 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of integers");
		System.out.println("N=");
		int n = sc.nextInt();
		System.out.println("Enter elements of array");
		int[] arr = new int[n];
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter no. of rotations");
		System.out.println("p=");
		int p = sc.nextInt();
		System.out.println("Enter no. of queries");
		System.out.println("q=");
		int q = sc.nextInt();
		int[] query = new int[q];
		for(int i=0; i<q; i++) {
			System.out.println("q"+(i+1)+"=");
			query[i] = sc.nextInt();
		}
		for(int i=0; i<p; i++) {
			int temp = arr[n-1];
			for(int j=n-1; j>0; j--) {
				arr[j] = arr[j-1];
			}
			arr[0] = temp;
		}
		for(int i=0; i<n; i++) {
			System.out.print(arr[query[i]]+" ");
		}

	}

}

import java.util.Scanner;

public class Q5 {
	
	int r1_BL_x, r1_BL_y, r1_BR_x, r1_BR_y, r1_TL_x, r1_TL_y, r1_TR_x, r1_TR_y;//r1 = rectangle 1, BL = bottom left, BR = bottom right, TL = top left, TR = top right
	int r2_BL_x, r2_BL_y, r2_BR_x, r2_BR_y, r2_TL_x, r2_TL_y, r2_TR_x, r2_TR_y;//r2 = rectangle 2

	 boolean doIntersect() {
		 if (r1_TL_x == r1_BR_x || r1_TL_y == r1_BR_y || r2_TL_x == r2_BR_x || r2_TL_y == r2_BR_y)
         {
                 // cannot be a straight line
             return false;
         }
 
 
       
     if (r1_TL_x >= r2_BR_x || r2_TL_x >= r1_BR_x) {
         return false;
     }

     if (r1_TL_y <= r2_BR_y || r2_TL_y <= r1_BR_y) {
         return false;
     }
		return true;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the coordinates of rectangle 1");
		System.out.println("Bottom left x, bottom left y");
		int r1_BL_x = sc.nextInt();
		int r1_BL_y = sc.nextInt();
		System.out.println("Bottom right x, bottom right y");
		int r1_BR_x = sc.nextInt();
		int r1_BR_y = sc.nextInt();
		System.out.println("Top left x, top left y");
		int r1_TL_x = sc.nextInt();
		int r1_TL_y = sc.nextInt();
		System.out.println("Top right x, top right y");
		int r1_TR_x = sc.nextInt();
		int r1_TR_y = sc.nextInt();
		System.out.println("Enter the coordinates of rectangle 2");
		System.out.println("Bottom left x, bottom left y");
		int r2_BL_x = sc.nextInt();
		int r2_BL_y = sc.nextInt();
		System.out.println("Bottom right x, bottom right y");
		int r2_BR_x = sc.nextInt();
		int r2_BR_y = sc.nextInt();
		System.out.println("Top left x, top left y");
		int r2_TL_x = sc.nextInt();
		int r2_TL_y = sc.nextInt();
		System.out.println("Top right x, top right y");
		int r2_TR_x = sc.nextInt();
		int r2_TR_y = sc.nextInt();
		
		Q5 ob = new Q5();
		
		if(ob.doIntersect()==true) {
			System.out.println("The rectangles intersect each other");
		} else {
			System.out.println("The rectangles don't intersect each other");
		}

	}

}

import java.util.Scanner;

public class Q50 {
	static int isPrime(int num) {
		for(int i=2; i<=Math.sqrt(num); i++) {
			if(num%i==0) {
				return 0;
			}
		}
		return 1;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.println("Enter the 1st integer");
        int m = sc.nextInt();
        System.out.println("Enter the 2nd integer");
        int n = sc.nextInt();
        int count = 0;
        for(int i=m+1; i<n; i++) {
        	if(isPrime(i)==1) {
        		count++;
        		System.out.print(i+" ");
        	}
        }
        System.out.println();
        System.out.println("Total no. of prime factors = "+count);
	}

}

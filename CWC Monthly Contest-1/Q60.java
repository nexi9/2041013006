import java.util.Scanner;

public class Q60 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no. of rows and columns of 1st matrix");
        int row1 = sc.nextInt();
        int col1 = sc.nextInt();
        System.out.println("Enter the no. of rows and columns of 2nd matrix");
        int row2 = sc.nextInt();
        int col2 = sc.nextInt();
        int[][] mat1 = new int[row1][col1];
        int[][] mat2 = new int[row2][col2];
        System.out.println("Enter the elements of 1st matrix");
        for(int i=0; i<row1; i++) {
        	for(int j=0; j<col1; j++) {
        		mat1[i][j] = sc.nextInt();
        	}
        }
        System.out.println("Enter the elements of 2nd matrix");
        for(int i=0; i<row2; i++) {
        	for(int j=0; j<col2; j++) {
        		mat2[i][j] = sc.nextInt();
        	}
        }
        if(col1 != row2) {
        	System.out.println("They can't be multiplied");
        }
        System.out.println("Product =");
        int[][] pro = new int[row1][col2];
        for(int i=0; i<row1; i++) {
        	for(int j=0; j<col2; j++) {
        		for(int k=0; k<col1; k++) {
        			pro[i][j] += mat1[i][k]*mat2[k][j];
        		}
        	}
        }
        for(int i=0; i<row1; i++) {
        	for(int j=0; j<col2; j++) {
        		System.out.print(pro[i][j]+" ");
        	}
        	System.out.println();
        }
        
        
	}

}

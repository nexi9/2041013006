import java.util.Scanner;

public class Q73 {
	
	static int factorial(int n) {
		int pro = 1;
		for(int i=1; i<=n; i++) {
			pro *= i;
		}
		return pro;
	}

	public static void main(String[] args) {
		/* sin(x) = x-x^3/3! + x^5/5! - x^7/7! + ...
		 * can be written as
		 * sin(x) = ((-1)^i*x^(2i+1))/(2i+1)!
		 * where i=0 to infinite
		 */

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of x for sin x ");
		int x = sc.nextInt();
		System.out.println("Enter the value upto which you want to calculate");
		int n = sc.nextInt();
		double sum = 0;
		for(int i=0; i<n; i++) {
			sum += (Math.pow(-1, i)*Math.pow(x, 2*i+1))/factorial(2*i+1);
		}
		System.out.println(sum);
	}

}

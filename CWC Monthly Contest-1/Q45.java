import java.util.Scanner;

public class Q45 {
	static int ToBinary(int n) {
		int bin = 0;
		while(n>0) {
			int rem = n%10;
			bin = bin*10 + rem;
			n=n/10;
		}
		return bin;
	}
	
    static int getDecimal(int n){  
	    int decimal = 0;  
	    int c = 0;
	    while(true){  
	      if(n == 0){  
	        break;  
	      } else {  
	          int temp = n%10;  
	          decimal += temp*Math.pow(2, c);  
	          n = n/10; 
	          c++;
	      }  
	    }  
	    return decimal;  
	}  

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter an integer");
		int n = sc.nextInt();
		int bin = ToBinary(n);
		String binary = Integer.toString(bin);
		String minBinary = "";
		int count0 = 0, count1 = 0;
		for(int i=0; i<binary.length(); i++) {
			if(binary.charAt(i)=='1') {
				count1++;
			} else {
				count0++;
			}
		}
		for(int i=0; i<count0; i++) {
			minBinary += '0';
		}
		for(int i=0; i<count1; i++) {
			minBinary += '1';
		}
		int minBin = Integer.parseInt(minBinary);
		int dec = getDecimal(minBin);
		System.out.println(dec);

	}

}

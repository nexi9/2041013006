import java.util.Scanner;

public class Q32 {
	
	static int fact(int k) {
		int factorial = 1;
		for(int i=1; i<=k; i++) {
			factorial *= i;
		}
		return factorial;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter m and n");
		int m = sc.nextInt();
		int n = sc.nextInt();
		int M = fact(m);//m!
		int N = fact(n);//n!
		int NM = fact(n-m);//(n-m)!
		int c = N/(M*(NM));
		System.out.println("nCm = "+c);

	}

}

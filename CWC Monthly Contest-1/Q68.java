import java.util.Scanner;

public class Q68 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of array");
		int n = sc.nextInt();
		System.out.println("Enter elements of array");
		int[] arr = new int[n];
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the no. of elements you want to move ");
		int m = sc.nextInt();
		for(int i=0; i<m; i++) {
			int temp = arr[0];
			for(int j=0; j<n-1; j++) {
				arr[j] = arr[j+1];
			}
			arr[n-1] = temp;
		}
		for(int i=0; i<n; i++) {
			System.out.print(arr[i]+" ");
		}
		

	}

}

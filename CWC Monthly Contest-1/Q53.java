import java.util.Scanner;

public class Q53 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of terms upto which you want to generate");
		int n = sc.nextInt();
		int k = 1;
		for(int i=0; i<n; i++) {
			System.out.print(k*k+","+k*k*k+",");
			k++;
		}

	}

}

import java.util.Scanner;

public class Q88 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter starting value");
		int start = sc.nextInt();
		System.out.println("Enter ending value");
		int end = sc.nextInt();
		System.out.println("Enter the amount of integers you want to generate");
	    int n = sc.nextInt();
		for(int i=0; i<n; i++) {
			System.out.print(start+(int)(Math.random()*((end-start)+1))+" ");
		}

	}

}


public class Q91 {
	
	public static void main(String[] args) {
		long n = 600851475143l;
		for(long i=2; i<Math.sqrt(n); i++) {
			if(n%i==0) {
				n = n/i;
			}
		}
		System.out.println("Max prime factor = "+n);
		

	}

}

import java.util.Scanner;

public class Q58 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter roman number");
		String roman = sc.next();
		int l = roman.length();
		roman = roman+" ";
		int res = 0;
		for(int i=0; i<l; i++) {
			char ch = roman.charAt(i);
			char nextCh = roman.charAt(i+1);
			
			if(ch=='I') {
				if(nextCh=='X') {
					res += 9;
				} else if(nextCh=='V') {
					res += 4; 
				} else {
					res += 1;
				}
			} else if (ch=='V') {
				res += 5;
			} else if (ch=='X') {
				if(nextCh == 'L') {
					res += 40;
				} else if (nextCh=='C') {
					res += 90;
				} else {
					res += 10;
				}
			} else if (ch=='C') {
				if(nextCh=='D') {
					res += 400;
				} else if (nextCh=='M') {
					res += 900;
				} else {
					res += 100;
				}
			} else if (ch=='L') {
				res += 50;
			} else if (ch=='D') {
				res += 500;
			} else if(ch=='M') {
				res +=1000;
			}
		}
		System.out.println(res);
	}

}

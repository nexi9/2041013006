import java.util.Scanner;

public class Q87 {
	
	static float roundUp(float n) {
		float c = (float) Math.ceil(n);
		float f = (float) Math.floor(n);
		if((c-n)>(n-f)) {
			return f;
		} else if((n-f)>(c-n)) {
			return c;
		}
		return c;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input a float number");
		float num = sc.nextFloat();
		System.out.println("The rounded value of "+num+" is: "+roundUp(num));

	}

}


public class Q55 {
	double real, imag;
	
	Q55(double real, double imag) {
		this.real = real;
		this.imag = imag;
	}
	
	static Q55 add(Q55 x,Q55 y) {
		Q55 temp = new Q55(0,0);
		temp.real = x.real + y.real;
		temp.imag = x.imag = y.imag;
		
		return temp;
	}

	public static void main(String[] args) {
		Q55 c1 = new Q55(4.3, 8.9);
		Q55 c2 = new Q55(7.6, 9.0);
		System.out.println("Sum = "+add(c1,c2).real+"+"+add(c1,c2).imag+"i");

	}

}

import java.util.Scanner;

public class Q62 {
	
	static boolean isPrime(int n) {
		for(int i=2; i<=Math.sqrt(n);i++) {
			if(n%i==0) {
				return false;
			}
		}
		return true;
	}
	
	static boolean isPalindrome(int n) {
		int sum = 0;
		int temp = n;
		while(n>0) {
			int rem = n%10;
			sum = sum*10 + rem;
			n = n/10;
		}
		if(temp==sum) {
			return true;
		}
		return false;
	}
	
	static boolean primePal(int n) {
		if(isPrime(n)==true && isPalindrome(n)==true) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of n");
		int n = sc.nextInt();
		System.out.println("Prime Paindromes upto n:");
		for(int i=2; i<=n; i++) {
			if(primePal(i)==true) {
				System.out.print(i+",");
			}
		}
		

	}

}

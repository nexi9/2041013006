
public class Q98 {
	
	static int factorial(int n) {
		int fact=1;
		if(n==0) {
			return 1;
		}
		for(int i=2; i<=n; i++) {
			fact *= i;
		}
		return fact;
	}
	
	static boolean isEqual(int num) {
		int temp = num;
		int sum=0;
		if(num==0) {
			return false;
		}
		while(num>0) {
			int rem = num%10;
			sum += factorial(rem);
			num = num/10;
		}
		if(temp==sum) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		int sum = 0;
		for(int i=0; i<=100000000; i++) {
			if(isEqual(i)==true) {
				System.out.print(i+" ");
			}
		}
		

	}

}

import java.util.Scanner;

public class Q23 {
	
	static int[] merge(int[] x, int[] y, int[] z) {
		int i=0;
		int j=0;
		for(int k=0;k<z.length;k++) {
			if(k%2==0) {
				z[k]=x[i];
				i++;
			}
			else {
				z[k]=y[j];
				j++;
			}
		}
		for(int k=0;k<z.length;k++)
		{
			System.out.print(z[k]+" ");
		}
		return z;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array");
		int n = sc.nextInt();
		int[] arr = new int[n];
		int[] brr = new int[n];
		System.out.println("Enter the elements of first array");
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the elements of second array");
		for(int i=0; i<n; i++) {
			brr[i] = sc.nextInt();
		}
		System.out.println("The two arrays are");
		for(int i=0; i<n; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		for(int i=0; i<n; i++) {
			System.out.print(brr[i]+" ");
		}
		System.out.println();
		int[] crr = new int[n+n];
		System.out.println("The merged array is");
		merge(arr,brr,crr);
		

	}

}


public class Q66 {

	public static void main(String[] args) {
		int[] arr = new int[100];
		arr[0] = 0;
		arr[1] = 0;
		int pow = 1;
		for(int i=2; i<99; i=i+2) {
			arr[i] = 7*pow;
			arr[i+1] = 6*pow;
			pow++;
		}
		System.out.println("15th term of the series = "+arr[14]);

	}

}

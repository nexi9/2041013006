import java.util.Scanner;

public class Q78 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of array");
		int n = sc.nextInt();
		System.out.println("Enter elements of array");
		int[] arr = new int[n];
		for(int i=0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the index of element you want to swap");
		int p = sc.nextInt();
		for(int i=1; i<=n; i++) {
			if(i-1==p) {
				int temp = arr[i-1];
				arr[i-1] = arr[n-p-1];
				arr[n-p-1] = temp;
			}
		}
		for(int i=0; i<n; i++) {
			System.out.print(arr[i]+" ");
		}
		

	}

}

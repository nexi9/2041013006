import java.util.Scanner;

public class Q9 {
	
	static int factorial(int n) {
		 if (n == 0)
	          return 1;
	          
	        return n*factorial(n-1);
    }
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter n");
		int n = sc.nextInt();
		long sum=0;
		long fact=factorial(n);
	    while(fact>0) {
	    	long rem=fact%10;
	    	sum+=rem;
	    	fact=fact/10;
	    }
		System.out.println(sum);

	}

}

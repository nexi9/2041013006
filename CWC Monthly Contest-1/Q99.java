
public class Q99 {
	
	static int isPrime(int num) {
		for(int i=2; i<=Math.sqrt(num); i++) {
			if(num%i==0) {
				return 0;
			}
		}
		return 1;
	}

	public static void main(String[] args) {
		for(int i=2; i<1000; i++) {
			if(isPrime(i)==1) {
				String num = Integer.toString(i);
				String temp = num;
				for(int j=0; j<num.length(); j++) {
					 temp = temp.charAt(temp.length() - 1) + temp;
		             temp = temp.substring(0, temp.length() - 1);
		             if(isPrime(Integer.parseInt(temp))==0) {
							break;
						} else {
							System.out.print(temp+" ");
						}
				}
				
 			}
		}

	}

}


public class Q54 {

	public static void main(String[] args) {
		for(int i=0; i<5; i++) {
			for(int j=0; j<=i; j++) {
				if(i==1||i==3) {
					continue;
				} else {
					System.out.print(" ");
				}
				
			}
			for(int j=i; j<5; j++) {
				if(i==1||i==3) {
					System.out.print("");
				} else {
					System.out.print("* ");
				}
					
			}
			if(i==1||i==3) {
				continue;
			} else {
				System.out.println();
			}
		}
		
		for(int i=0; i<5; i++) {
			for(int j=5-i-1; j>=0; j--) {
				if(i==1||i==3) {
					continue;
				} else {
					System.out.print(" ");
				}
				
			}
			for(int j=i; j>=0; j--) {
				if(i==1||i==3) {
					System.out.print("");
				} else {
					System.out.print("* ");
				}
					
			}
			if(i==1||i==3) {
				continue;
			} else {
				System.out.println();
			}
		}

	}

}

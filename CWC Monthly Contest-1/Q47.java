import java.util.Scanner;

public class Q47 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int n = sc.nextInt();
		int temp = n;
		int sum = 0;
		int r;
		while(n>0) {
			r = n%10;
			sum = sum + r*r*r;
			n = n/10;
		}
		if(sum == temp) {
			System.out.println("It is an Armstrong number");
		} else {
			System.out.println("It is not an Armstrong number");
		}

	}

}

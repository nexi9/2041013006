
public class Q92 {

	public static void main(String[] args) {
		int sum=0;
		for(int i=1; i<=100; i++) {
			int sq = i*i;
			sum += sq;
		}
		System.out.println("Sum of squares = "+sum);
        int sum2 = 0;
        for(int i=0; i<100; i++) {
        	sum2 += i;
        }
        System.out.println("Square of the sum = "+sum2*sum2);
        System.out.println("Difference = "+Math.abs(sum-(sum2*sum2)));
	}

}

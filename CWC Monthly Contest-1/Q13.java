import java.util.Scanner;

public class Q13 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the total no. of questions asked:");
		int n = sc.nextInt();
		String[] asker = new String[n];
		String[] coord = new String[n];
		for(int i=0; i<n; i++) {
			System.out.println("Enter the asker's and coordinator's name for the "+(i+1)+" one");
			 asker[i] = sc.next();
			 coord[i] = sc.next();
		}
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("\tAsker\t\t\tQuery Solved by");
		System.out.println("\t-----\t\t\t----------");
		for(int i=0; i<n; i++) {
			System.out.println("\t"+asker[i]+"\t\t\t"+coord[i]);
		}
		System.out.println("----------------------------------------------------------------------------");

	}

}

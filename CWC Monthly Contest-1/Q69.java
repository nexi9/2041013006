import java.util.Scanner;

public class Q69 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int sum=0;
		for(int i=2; i<=n; i=i+2 ) {
			if(n%i==0) {
				sum += i;
			}
		}
		System.out.println("Sum of all even factors of "+n+" = "+sum);

	}

}

import java.util.Scanner;

public class Q67 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of 1st array");
		int a1 = sc.nextInt();
		int[] A1 = new int[a1];
		System.out.println("Enter elements of 1st array");
		for(int i=0; i<a1; i++) {
			A1[i] = sc.nextInt();
		}
		System.out.println("Enter length of 2nd array");
		int a2 = sc.nextInt();
		int[] A2 = new int[a2];
		System.out.println("Enter elements of 2nd array");
		for(int i=0; i<a2; i++) {
			A2[i] = sc.nextInt();
		}
		System.out.println("Enter length of 3rd array");
		int a3 = sc.nextInt();
		int[] A3 = new int[a3];
		System.out.println("Enter elements of 3rd array");
		for(int i=0; i<a3; i++) {
			A3[i] = sc.nextInt();
		}
		int i=0,j=0,k=0;
		System.out.println("Common elements are:");
		while(i<a1 && j<a2 && k<a3) {
			if(A1[i]==A2[j] && A1[i]==A3[k]) {
				System.out.print(A1[i]+" ");
				i++;
				j++;
				k++;
			} else if(A1[i]<A2[j]) {
				i++;
			} else if(A2[j]<A3[k]) {
				j++;
			} else {
				k++;
			}
		}
		
		

	}

}

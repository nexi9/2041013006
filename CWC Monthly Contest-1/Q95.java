
public class Q95 {
	
	static int isPrime(int num) {
		for(int i=2; i<Math.sqrt(num); i++) {
			if(num%i==0) {
				return 0;
			}
		}
		return 1;
	}

	public static void main(String[] args) {
		long sum = 0;
		for(int i=2; i<2000000; i++) {
			if(isPrime(i)==1) {
				sum += i;
			}
		}
		System.out.println("Sum of primes below 2 million = "+sum);

	}

}

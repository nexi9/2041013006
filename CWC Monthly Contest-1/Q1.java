import java.util.Scanner;

public class Q1 {
	
	 static int perm(int n) {
		    if(n == 0) 
		    	return 0;
	        if (n == 1) 
	        	return 0;
	        if (n == 2) 
	        	return 1;
	        
	        return (n - 1)*(perm(n - 1)+perm(n - 2));
	    }
	     

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.println("n = ");
        int n = sc.nextInt();
        System.out.println(perm(n));
	}

}

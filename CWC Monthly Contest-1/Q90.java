import java.util.Scanner;

public class Q90 {
	
	static int reverse(int n) {
		int m=0;
		while(n>0) {
			int rem = n%10;
			m = m*10 + rem;
			n = n/10;
		}
		return m;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int n = sc.nextInt();
		System.out.println("It's reverse = "+reverse(n));

	}

}

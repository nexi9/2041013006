import java.util.Scanner;

public class Q43 {
	
	static void convert(int n) {
		int temp = n;
		System.out.println("Binary:");
		int[] binary = new int[100];
		int i = 0;
		while(n>0) {
			binary[i] = n%2;
			n = n/2;
			i++;
		}
		for(int j=i-1; j>=0; j--) {
			System.out.print(binary[j]);
		}
		System.out.println();
		
		n = temp;
		
		System.out.println("Hexadecimal:");
		
		int[] hexa = new int[100];
		int k = 0;
		while(n>0) {
			hexa[k] = n%16;
			n = n/16;
			k++;
		}
		for(int j=k-1; j>=0; j--) {
			if(hexa[j]>9) {
				System.out.print((char)(55+hexa[j]));
			} else {
				System.out.print(hexa[j]);
			}
		}
		System.out.println();
		
		n = temp;
		System.out.println("Octal:");
		
		int[] octa = new int[100];
		int l = 0;
		while(n>0) {
			octa[l] = n%8;
			n = n/8;
			l++;
		}
		for(int j=l-1; j>=0; j--) {
			System.out.print(octa[j]);
		}
		
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int n = sc.nextInt();
		convert(n);
		

	}

}

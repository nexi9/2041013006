import java.util.Scanner;

public class Q39 {
	
	static boolean isPrime(int n) {
		if(n<=1)
			return false;
		for(int i=2; i<=Math.sqrt(n); i++) {
			if(n%i==0)
				return false;
		}
		return true;
		
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0; i<T; i++) {
			int start = sc.nextInt();
			int end = sc.nextInt();
			for(int j=start; j<=end; j++) {
				if(isPrime(j)) {
					System.out.println(j);
				}
			}
			System.out.println();
			System.out.println();
		}

	}

}

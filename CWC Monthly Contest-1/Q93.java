import java.math.BigInteger;

public class Q93 {

	public static void main(String[] args) {
		
	 BigInteger big = new BigInteger("2");
	    big = big.pow(1000);
	    String n = big.toString();
	    int result = 0;
	    for(char i : n.toCharArray()) {
	        result += Integer.parseInt(String.valueOf(i));
	    }
	    System.out.println(result);
	}
}

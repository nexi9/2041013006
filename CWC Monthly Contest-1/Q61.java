import java.util.Scanner;

public class Q61 {
	
	static double squareRoot(int n) {
		double r = n;
		double con;
		while(true) {
			con = 0.5*(r+(n/r));	
			if(Math.abs(con-r)<1)
				break;
			r = con;
		}
		
		return con;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int n = sc.nextInt();
		System.out.println("Square root = "+squareRoot(n));

	}

}
